FROM gitlab-registry.cern.ch/industrial-controls/sw-infra/jenkins/master-master:latest

# Install FESA specific plugins, e.g. :
USER root
RUN /usr/local/bin/install-plugins.sh matrix-auth:2.6.5 ssh-slaves:1.31.5

USER 1001

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/usr/libexec/run"]

